import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mission } from './mission';

@Injectable({
  providedIn: 'root'
})
export class MissionService {
 
  private apiUrl = 'http://exemple.com/api/missions'; // Remplacez par votre URL API

  constructor(private http: HttpClient) { }

  getMissions(): Observable<any> {
    return this.http.get<any>('assets/team-data.json');
  }

  addMission(mission: Mission): Observable<any> {
    return this.http.post(this.apiUrl, mission);
  }

  updateMission(mission: Mission): Observable<any> {
    const url = `${this.apiUrl}/${mission.id}`;
    return this.http.put(url, mission);
  }

  deleteMission(id: number): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete(url);
  }
}
