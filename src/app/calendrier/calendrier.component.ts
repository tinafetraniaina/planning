import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { MissionService } from '../mission.service';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { MissionDialogComponent } from '../mission-dialog/mission-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-calendrier',
  templateUrl: './calendrier.component.html',
  styleUrls: ['./calendrier.component.css']
})
export class CalendrierComponent implements OnInit {
  events = [];
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,dayGridWeek'
    },
    initialView: 'dayGridMonth',
    plugins: [dayGridPlugin, interactionPlugin],
    eventClick: (arg) => this.handleEventClick(arg),
    eventDrop: (arg) => this.handleEventDrop(arg),
    eventResize: (arg) => this.handleEventResize(arg),
    eventMouseEnter: (arg) => this.handleEventMouseEnter(arg),
    eventMouseLeave: (arg) => this.handleEventMouseLeave(arg),
    eventContent: (arg) => this.eventContent(arg),
    events: this.events,
    editable: true
  };

  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  constructor(private missionService: MissionService, public modalService: NgbModal) { }

  ngOnInit(): void {
    this.loadMissions();
  }

  handleEventClick(info) {
    console.log(info.event)
    const modalRef = this.modalService.open(MissionDialogComponent);
    modalRef.componentInstance.data = { title: info.event.title, description: info.event.extendedProps.description, color: info.event.backgroundColor, startdate: info.event.startStr, enddate: info.event.endStr};

    modalRef.result.then((result) => {
      if (result !== 'delete') {
        // Logique pour update de la mission dans mission.service
        info.event.setProp('title', result.title);
        info.event.setExtendedProp('description', result.description);
        info.event.setProp('color', result.color);
        info.event.setDates(result.startdate, result.enddate);
      } else {
        // Logique pour supprimer la mission dans mission.service
        console.log('Mission deleted');
      }
    }, (reason) => {
      // Gestion des cas où le modal est fermé sans clic sur les boutons Enregistrer/Supprimer
      console.log('Modal dismissed:', reason);
    });
  }

  handleEventDrop(info) {
    console.log('Déplacement de l\'événement : ', info.event.title);
    console.log('Nouvelle date de début : ', info.event.start);
    console.log('Nouvelle date de fin : ', info.event.end);
  }

  handleEventResize(info) {
    console.log('Redimensionnement de l\'événement : ', info.event.title);
    console.log('Nouvelle date de début : ', info.event.start);
    console.log('Nouvelle date de fin : ', info.event.end);
  }

  handleEventMouseEnter(info) {
    const description = info.event.extendedProps.description;
    if (description) {
      const tooltip = document.createElement('div');
      tooltip.classList.add('tooltip-desc');
      tooltip.textContent = description;
      info.el.appendChild(tooltip);
    }
  }

  handleEventMouseLeave(info) {
    const tooltips = info.el.getElementsByClassName('tooltip-desc');
    while (tooltips.length > 0) {
      tooltips[0].parentNode.removeChild(tooltips[0]);
    }
  }

  loadMissions() {
    this.missionService.getMissions().subscribe(data => {
      // Process missions and add them to calendarEvents
      this.events = this.parseEvents(data.teamMembers);
      this.calendarOptions.events = this.events;
    });
  }

  parseEvents(teamMembers: any[]): any[] {
    let events = [];
    teamMembers.forEach(member => {
      member.missions.forEach(mission => {
        const event = {
          title: mission.title,
          start: mission.start,
          end: mission.end,
          color: mission.color,
          description: mission.description,
          memberName: member.name,
          memberPhoto: member.photo
        };
        events.push(event);
      });
    });
    return events;
  }

  eventContent(info) {
    // Créez un élément de conteneur pour le nom et la photo
    const container = document.createElement('div');

    // Ajoutez le nom du membre
    const nomElement = document.createElement('div');
    nomElement.textContent = info.event.extendedProps.memberName;
    container.appendChild(nomElement);

    // Ajoutez la photo du membre
    const photoElement = document.createElement('img');
    photoElement.src = `assets/images/${info.event.extendedProps.memberPhoto}`;
    photoElement.width = 40; // Définissez la largeur de la photo selon vos besoins
    photoElement.height = 40; // Définissez la hauteur de la photo selon vos besoins
    container.appendChild(photoElement);

    // Retournez le contenu personnalisé
    return { domNodes: [container] };
  }

  saveChanges() {
    const modifiedEvents = this.calendarComponent.getApi().getEvents();
    console.log('Changements apportés : ', modifiedEvents);
  }

  addMission() {
    const modalRef = this.modalService.open(MissionDialogComponent);
    modalRef.componentInstance.data = { title: '', description: '', color: '' };
    modalRef.result.then((result) => {
      if (result !== undefined) {
        // Logique pour sauvegarder la mission dans mission.service
        console.log('Mission saved:', result);
        const newEvent = {
          id: this.generateEventId(),
          title: result.title,
          start: result.startdate,
          end: result.enddate,
          color: result.color,
          description: result.description,
        };
        this.events = [...this.events, newEvent];
        this.calendarOptions.events = this.events;
      } else {
        // Logique pour supprimer la mission
        console.log('Mission deleted');
      }
    }, (reason) => {
      // Gestion des cas où le modal est fermé sans clic sur les boutons Enregistrer/Supprimer
      console.log('Modal dismissed:', reason);
    });
  }

  generateEventId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }
}
