import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mission-dialog',
  templateUrl: './mission-dialog.component.html',
  styleUrls: ['./mission-dialog.component.css']
})
export class MissionDialogComponent {

  @Input() data: any = {
    title: '',
    description: '',
    color: '',
    startdate: null,
    enddate: null
  };

  constructor(public activeModal: NgbActiveModal) {}

  onSaveClick(): void {
    // Logique de sauvegarde de la mission
    this.activeModal.close(this.data);
  }

  onDeleteClick(info: any): void {
    console.log(info)
    // Logique de suppression de la mission
    this.activeModal.close('delete');
  }

}
